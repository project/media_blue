WHAT IS Media Blue?
------------------------

Media Blue is a professional theme with a div based, fixed width, 3 column layout with a featured slideshow.

FEATURES
--------

 * Media Blue has an in build slideshow feature

 * To enable/configure slideshow do the following steps
   
   * Goto appearance/settings/media_blue.
   * Select Banner management and upload the slideshow images and click 'Save configuration'.
 
INSTALLATION
------------

 1. Download Media Blue from http://drupal.org/project/media_blue

 2. Unpack the downloaded files, take the folders and place them in your
    Drupal installation under one of the following locations:
      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configuration
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

    Note: you will need to create the "themes" folder under "sites/all/"
    or "sites/default/".

FURTHER READING
---------------

Full documentation on using Media Blue:
  http://www.freedrupalthemes.net/docs/media_blue

Drupal theming documentation in the Theme Guide:
  http://drupal.org/theme-guide
  
Media Blue demo site
  http://d7.freedrupalthemes.net/t/media_blue
